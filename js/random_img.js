(function () {
  var Movable = function (x, y) {
    this.pos = {
      x: x,
      y: y
    };
    this.move = function (x, y) {
      this.pos.x += x;
      this.pos.y += y;
    };
  }
  
  // ページ全体の高さを取得
  function bodyContentsHeight() {
    var h = Math.max.apply( null, [document.body.clientHeight , document.body.scrollHeight, document.documentElement.scrollHeight, document.documentElement.clientHeight] ); 
    return h;
  }

  function randomPosition() {
    let random_position_array = [
      // 右端
      Math.floor((Math.random() + 8) * (window.innerWidth / 10)),
      // 左端
      Math.floor((Math.random() * 2) * (window.innerWidth / 10))
    ];
    var img = Math.floor(Math.random() * 2);
    return random_position_array[img];
  }

  var random_img = [];
    
  for (let i = 0; i <= 20; i++) {
    random_img[i] = new Movable(
      randomPosition(),
      Math.floor(Math.random() * bodyContentsHeight())
    );
  }

  var img_list = [
    'fish_g250.png',
    'fish_r250.png',
    'fish_w250.png',
    'fish_blue250.png',
    'kani_r250.png',
    'kani_g250.png',
    'kani_b250.png'
  ];

  for (let i = 0; i <= 20; i++) {
    var img_num = Math.floor(Math.random() * img_list.length) + 0;
    document.write('<img class="d-none d-xl-block" style="height:100px; width:100px; opacity:0.6; position:absolute; top:' + random_img[i].pos.y + 'px; left:' + random_img[i].pos.x + 'px;" src="./img/'+img_list[img_num]+'">');
  }
})();