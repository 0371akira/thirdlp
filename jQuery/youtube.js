$(function (){
  var srcs = [];
  var images = [];
  var heights = [];
  var widths = [];
 
  // 全てのiframeタグに対して処理を実行
  $('iframe.fastyt').each(function(index, element) {
    // 属性を配列に格納
    srcs[index] = $(this).attr('data-src');
    heights[index] = $(this).attr('height');
    widths[index] = $(this).attr('width');
 
    // URL から動画 id のみを取得して文字連結をしてサムネイルを取得
    var id = srcs[index].match(/[\/?=]([a-zA-Z0-9_-]{11})[&\?]?/)[1];
    images[index] = '//img.youtube.com/vi/' + id + '/mqdefault.jpg';
 
    // iframeをサムネイル画像に置換
    $(this).after('<div class="yt mx-auto shadow"><div class="yt_play"><img src="' + images[index] + '" class="movie" width="' + widths[index] + '" height="' + heights[index] + '"></div></div>').remove();
    $('.yt').eq([index]).css("width", widths[index]);
    $('.yt').eq([index]).css("height", heights[index]);
  });
   
  $('.yt_play').each(function(index, element) {
    // サムネイルがクリックされた時の処理
    $(this).click(function (){
      // iframeに置換
      var autoplay;
      if (0 < srcs[index].indexOf("?")) {
        autoplay = '&';
      } else {
        autoplay = '?';
      }
      autoplay += "autoplay=1";
 
      $(this).replaceWith('<iframe src="' + srcs[index] + autoplay + '" allow="autoplay" frameborder="0" width="' + widths[index] + '" height="' + heights[index] + '"></iframe>');
    });
  });
});