$('.animated').waypoint({
  handler(direction) {
    var animatedPoint = $(this.element);
    if (direction === 'down') {
    //  animatedPoint.removeClass('fadeOutUp');
      animatedPoint.addClass('fadeIn');
    } else if (direction === 'up') {
      animatedPoint.removeClass('fadeIn');
    //  animatedPoint.addClass('fadeOutUp');
    }
  },
  offset: '95%',
});